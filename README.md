## Task

---
### Responsive Web Design Projects - Build a Survey Form

**Objective**: Build a CodePen.io app that is functionally similar to [this](https://codepen.io/freeCodeCamp/full/VPaoNP).

Fulfill the below user stories and get all the tests to pass. Give it your own personal style.

You can use HTML, JavaScript, and CSS to complete this project. Plain CSS is recommended because that is what the lessons have covered so far and you should get some practice with plain CSS. You can use Bootstrap or SASS if you choose. Additional technologies (just for example jQuery, React, Angular, or Vue) are not recommended for this project, and using them is at your own risk. Other projects will give you a chance to work with different technology stacks like React. We will accept and try to fix all issue reports that use the suggested technology stack for this project. Happy coding!

**_User Story #1:_** I can see a title with id="title" in H1 sized text.

**_User Story #2:_** I can see a short explanation with id="description" in P sized text.

**_User Story #3:_** I can see a form with id="survey-form".

**_User Story #4:_** Inside the form element, I am required to enter my name in a field with id="name".

**_User Story #5:_** Inside the form element, I am required to enter an email in a field with id="email".

**_User Story #6:_** If I enter an email that is not formatted correctly, I will see an HTML5 validation error.

**_User Story #7:_** Inside the form, I can enter a number in a field with id="number".

**_User Story #8:_** If I enter non-numbers in the number input, I will see an HTML5 validation error.

**_User Story #9:_** If I enter numbers outside the range of the number input, which are defined by the min and max attributes, I will see an HTML5 validation error.

**_User Story #10:_**  For the name, email, and number input fields inside the form I can see corresponding labels that describe the purpose of each field with the following ids: id="name-label", id="email-label", and id="number-label".

**_User Story #11:_**  For the name, email, and number input fields, I can see placeholder text that gives me a description or instructions for each field.

**_User Story #12:_**  Inside the form element, I can select an option from a dropdown that has a corresponding id="dropdown".

**_User Story #13:_**  Inside the form element, I can select a field from one or more groups of radio buttons. Each group should be grouped using the name attribute.

**_User Story #14:_**  Inside the form element, I can select several fields from a series of checkboxes, each of which must have a value attribute.

**_User Story #15:_**  Inside the form element, I am presented with a textarea at the end for additional comments.

**_User Story #16:_**  Inside the form element, I am presented with a button with id="submit" to submit all my inputs.

You can build your project by forking [this](https://codepen.io/freeCodeCamp/full/VPaoNP) CodePen pen. Or you can use [this CDN link](https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js) to run the tests in any environment you like

Once you're done, submit the URL to your working project with all its tests passing.

---

## Solution

---

### Link

[pdimp / freeCodeCamp / Responsive Web Design / Survey Form](https://pdimp.gitlab.io/freeCodeCamp/Responsive-Web-Design/Survey-Form)

---

### Screenshots

![](screenshots/0.jpg)
